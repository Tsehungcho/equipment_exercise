﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EquipmentExercise
{
    public partial class SearchPanel : Form
    {
        string hierarchyScope = string.Empty;
        string ID = string.Empty;
        int pIndex = 0;

        // to indicate either it a create page or search page
        public SearchPanel(int pageIndex)
        {
            InitializeComponent();

            if (pageIndex == 1)
            {
                pIndex = 1;
                this.Text = "Create Page";
            }
            else
            {
                pIndex = 0;
                this.Text = "Search Page";
            }
        }

        private void button1_Click(object sender, EventArgs e)  
        {
            // some checker to checker both fields are not empty
            if (textBox1.Text.Trim() == "") { MessageBox.Show("Please fill in a Hierarchy Scope value"); }
            else if (textBox2.Text.Trim() == "") { MessageBox.Show("Please fill in a ID value"); }
            else
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
                    {
                        conn.Open();

                        string cmdString = "SELECT COUNT(NUMBER) from equipmentobject where HierarchyScope = '" + textBox1.Text.Trim() + "' and ID = '" + textBox2.Text.Trim() + "';";
                        SqlCommand cmd = new SqlCommand(cmdString, conn);


                        if (pIndex == 1) // create 
                        {
                            if (Convert.ToInt32(cmd.ExecuteScalar()) > 0)
                            {
                                MessageBox.Show("The equipment already exists in the database!!");
                            }
                            else
                            {
                               string cmdString2 = "INSERT INTO equipmentobject (HIERARCHYSCOPE,ID,EQUIPMENTLEVEL,DESCRIPTION) VALUES ('" + textBox1.Text + "','" + textBox2.Text + "',1,'');";
                                SqlCommand cmd2 = new SqlCommand(cmdString2, conn);
                                cmd2.ExecuteNonQuery();
 
                                hierarchyScope = textBox1.Text;
                                ID = textBox2.Text;
                                conn.Close();
                                this.Close();
                            }
                        }
                        else // search (
                        {
                            if (Convert.ToInt32(cmd.ExecuteScalar()) == 0)
                            {
                                MessageBox.Show("There no such equipment in the database!!");
                            }
                            else
                            {
                                hierarchyScope = textBox1.Text;
                                ID = textBox2.Text;
                                conn.Close();
                                this.Close();
                            }
                        }
                        
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        // return the hierarchy scope value
        public string getHierarchyScope()
        {
            return hierarchyScope;
        }

        // return the ID value
        public string getID()
        {
            return ID;
        }
    }
}
