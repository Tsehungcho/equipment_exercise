﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EquipmentExercise
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panel1.Visible = false;
            populateCombo();
        }

        // get unique ID for the equipment property
        private int gettotalID(string guid)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "SELECT COUNT(NUMBER) FROM EQUIPMENTPROPERTY where GROUPID = '" + guid + "';";
                SqlCommand cmd = new SqlCommand(cmdString, conn);

                int counter = Convert.ToInt32(cmd.ExecuteScalar());

                conn.Close();
                return counter + 1;
            }
        }

        // with the equipment level description, get the number associate with it
        private string getEquipmentLevelNumber(string desc)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "SELECT NUMBER, DESCRIPTION FROM EQUIPMENTLEVEL where DESCRIPTION = '" + desc + "';";
                SqlCommand cmd = new SqlCommand(cmdString, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    return reader["NUMBER"].ToString();
                }

                conn.Close();
                return "";
            }
        }

        // get the equpment level description when the number is provide
        private string getEquipmentLevel(string index)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "SELECT NUMBER, DESCRIPTION FROM EQUIPMENTLEVEL where NUMBER = " + index;
                SqlCommand cmd = new SqlCommand(cmdString, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    return reader["DESCRIPTION"].ToString();
                }

                conn.Close();
                return "";
            }
        }

        // populate the comboBox to display all the fields for equipment level
        private void populateCombo()
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "SELECT DESCRIPTION FROM EQUIPMENTLEVEL";
                SqlCommand cmd = new SqlCommand(cmdString, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    comboBox1.Items.Add(reader["DESCRIPTION"].ToString());
                }

                conn.Close();
            }
        }

        // clear up all the previoius value
        private void clearScreen()
        {
            label6.Text = string.Empty;
            label7.Text = string.Empty;
            label8.Text = string.Empty;
            label11.Text = string.Empty;
            label12.Text = string.Empty;
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;
        }

        // when the create button is clicked on the menu item
        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchPanel CreatePage = new SearchPanel(1);
            CreatePage.ShowDialog();
            clearScreen();

            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "SELECT * from equipmentobject where HierarchyScope = '" + CreatePage.getHierarchyScope().Trim() + "' and ID = '" + CreatePage.getID().Trim() + "'; ";
                SqlCommand cmd = new SqlCommand(cmdString, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    label8.Text = reader["NUMBER"].ToString();
                }
                conn.Close();
            }

            label6.Text = CreatePage.getHierarchyScope();
            label7.Text = CreatePage.getID();
            panel1.Visible = true;
        }

        // when the search button is clicked in the menu item
        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchPanel searchPage = new SearchPanel(0);
            searchPage.ShowDialog();
            clearScreen();

            label6.Text = searchPage.getHierarchyScope();
            label7.Text = searchPage.getID();
            panel1.Visible = true;

            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                // populate equipment object
                string cmdString = "SELECT * from equipmentobject where HierarchyScope = '" + searchPage.getHierarchyScope().Trim() + "' and ID = '" + searchPage.getID().Trim() + "';";
                SqlCommand cmd = new SqlCommand(cmdString, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    comboBox1.Text = getEquipmentLevel(reader["EQUIPMENTLEVEL"].ToString());
                    textBox1.Text = reader["DESCRIPTION"].ToString();
                    label8.Text = reader["NUMBER"].ToString();
                }
                reader.Close();

                string cmdString2 = "SELECT * from equipmentproperty where VALUE != '' AND GROUPID = '" + label8.Text + "' ORDER BY NUMBER;";
                SqlCommand cmd2 = new SqlCommand(cmdString2, conn);
                SqlDataReader reader2 = cmd2.ExecuteReader();

                // populate property
                while (reader2.Read())
                {
                    label11.Text = reader2["ID"].ToString();
                    textBox2.Text = reader2["VALUE"].ToString();
                    textBox3.Text = reader2["DESCRIPTION"].ToString();
                    break;
                }

                reader2.Close();

                // populate sub property
                string cmdString3 = "SELECT * from equipmentproperty where VALUE = '' AND GROUPID = '" + label8.Text + "' ORDER BY NUMBER;";
                SqlCommand cmd3 = new SqlCommand(cmdString3, conn);
                SqlDataReader reader3 = cmd3.ExecuteReader();

                while (reader3.Read())
                {
                    label12.Text = reader3["ID"].ToString();
                    textBox4.Text = reader3["DESCRIPTION"].ToString();
                    break;
                }

                reader3.Close();
                conn.Close();
            }
        }

        // save all the data into he equipmentobject
        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "UPDATE equipmentobject SET equipmentLevel = " + getEquipmentLevelNumber(comboBox1.Text)  + " , DESCRIPTION = '" +  textBox1.Text.Trim() + "' where HierarchyScope = '" + label6.Text + "' and ID = '" + label7.Text + "';";
                SqlCommand cmd = new SqlCommand(cmdString, conn);
                cmd.ExecuteNonQuery();

                MessageBox.Show("New equipment created.");
                conn.Close();
            }
        }

        // add equipment property
        private void button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "INSERT INTO equipmentproperty(ID, DESCRIPTION, VALUE, GROUPID) VALUES ('" + gettotalID(label8.Text).ToString() + "','" + textBox3.Text + "','" + textBox2.Text + "','" + label8.Text + "');" ;
                SqlCommand cmd = new SqlCommand(cmdString, conn);
                cmd.ExecuteNonQuery();

                MessageBox.Show("New Equipment property created.");
                conn.Close();
            }
        }

        // add sub equipment property
        private void button4_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                conn.Open();

                string cmdString = "INSERT INTO equipmentproperty(ID, DESCRIPTION, VALUE, GROUPID) VALUES ('" + gettotalID(label8.Text).ToString() + "','" + textBox4.Text + "','','" + label8.Text + "');";
                SqlCommand cmd = new SqlCommand(cmdString, conn);
                cmd.ExecuteNonQuery();

                MessageBox.Show("New Equipment sub property created.");
                conn.Close();
            }
        }

        // browse next available equipment property for the specify equipment
        private void button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                int next = 0;
                int checker = 0;
                conn.Open();

                if (label11.Text != "") { next = Convert.ToInt32(label11.Text); } 

                string cmdString = "SELECT * FROM EQUIPMENTPROPERTY where ID > " + next + " AND VALUE != '' AND GROUPID = '" + label8.Text + "' ORDER BY NUMBER;";
                SqlCommand cmd = new SqlCommand(cmdString, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    checker = -1;
                    label11.Text = reader["ID"].ToString();
                    textBox2.Text = reader["VALUE"].ToString();
                    textBox3.Text = reader["DESCRIPTION"].ToString();
                    break;
                }
                reader.Close();

                if ( checker == 0)
                {
                    cmdString = "SELECT * FROM EQUIPMENTPROPERTY where VALUE != '' AND GROUPID = '" + label8.Text + "' ORDER BY NUMBER;";
                    cmd = new SqlCommand(cmdString, conn);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        checker = -1;
                        label11.Text = reader["ID"].ToString();
                        textBox2.Text = reader["VALUE"].ToString();
                        textBox3.Text = reader["DESCRIPTION"].ToString();
                        break;
                    }
                    reader.Close();
                }

                conn.Close();
            }
        }


        // browse next available equipment sub property for the equipment
        private void button5_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-B7GPCFE;Initial Catalog=Equipment;Integrated Security=True"))
            {
                int next = 0;
                int checker = 0;
                conn.Open();

                if (label12.Text != "") { next = Convert.ToInt32(label12.Text); }

                string cmdString = "SELECT * FROM EQUIPMENTPROPERTY where ID > " + next + " AND VALUE = '' AND GROUPID = '" + label8.Text + "' ORDER BY NUMBER;";
                SqlCommand cmd = new SqlCommand(cmdString, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    checker = -1;
                    label12.Text = reader["ID"].ToString();
                    textBox4.Text = reader["DESCRIPTION"].ToString();
                    break;
                }
                reader.Close();

                if (checker == 0)
                {
                    cmdString = "SELECT * FROM EQUIPMENTPROPERTY where VALUE = '' AND GROUPID = '" + label8.Text + "' ORDER BY NUMBER;";
                    cmd = new SqlCommand(cmdString, conn);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        checker = -1;
                        label12.Text = reader["ID"].ToString();
                        textBox4.Text = reader["DESCRIPTION"].ToString();
                        break;
                    }
                    reader.Close();
                }

                conn.Close();
            }
        }
    }
}
